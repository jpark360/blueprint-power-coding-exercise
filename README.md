# Expected Value of Solar Energy Generated from Given Solar Installations

This tool models the amount of solar energy generated from a given solar installation and the expected value of this generated electricity over the course of a year. Information is pulled from the PVWatts API (​https://pvwatts.nrel.gov/​)

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). The backend is built on top of Python and Flask. 

# Server
To run the backend server, you will require Flask and Flask-Cors, 

```
  pip install Flask Flask-Cors
```

To run the server, navigate to the api folder and run the app.py script

```
  cd api
  python app.py
```

Server should be running on localhost:5000

# Client
To run the client, navigate to the client folder and npm install the required packages. 

```
  cd client
  npm install
```

To begin the client, type in the following command within the client folder

```
  npm start
```

Client should be running on localhost:3000

# Verifying data
The [Server](http://localhost:5000/pvwatts/40.74/-73.99/25/0/13/0/25/180) pulls from the [PVWatt API](https://developer.nrel.gov/api/pvwatts/v6.json?api_key=o15ui0rAVgIqglpcZPpDUmzCbtjI9ElWbrvJnVzt&lat=40.74&lon=-73.99&system_capacity=25&module_type=0&losses=13&array_type=0&tilt=25&azimuth=180). The data from these two sources should be the same.



# Notes
- Due to both the server and client being run on local servers, it might help to install a [Chrome Extension](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi) to bypass the CORS issues. 
- Rate ($/kWh) does not dynamically load in inputs component.
- Inputs are not required yet, as there are defaults within the system. The blank "Select" UI is a little confusing for the UX though.
- Need to fix workaround for default state in homepage, inputsarea, solarinputs components. Hacky
- Should probably add react-router to make the navigation easier.