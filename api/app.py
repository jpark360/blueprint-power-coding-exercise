from flask import Flask, json, send_file
from flask_cors import CORS
import requests, csv, datetime, os
from pathlib import Path

API_KEY = "o15ui0rAVgIqglpcZPpDUmzCbtjI9ElWbrvJnVzt"
BASE_URL = "https://developer.nrel.gov/"
PVWATT_URL = "api/pvwatts/v6.json?"
ELECRATE_URL = "api/utility_rates/v3.json?"

CSV_FILE = "api_ca"

# start a flask App and open it up to CORS. 
app = Flask(__name__)
CORS(app)

#pulls solar radiation, ac energy, and electricity rate data from PVWatts
@app.route("/pvwatts/<lat>/<lon>/<dc>/<mt>/<sl>/<at>/<ti>/<az>")
def getPVWatts(lat, lon, dc, mt, sl, at, ti, az):
    pvwatt_api_url = BASE_URL+PVWATT_URL
    pv_parameters = {
        "format" : "json",
        "api_key" : API_KEY,
        "lat" : lat,
        "lon" : lon,
        "system_capacity" : dc,
        "module_type" : mt,
        "losses" : sl,
        "array_type" : at,
        "tilt" : ti,
        "azimuth" : az
    }

    elecRate_api_url = BASE_URL+ELECRATE_URL
    elec_parameters = {
        "format" : "json",
        "api_key" : API_KEY,
        "lat" : lat,
        "lon" : lon
    }

    pv = requests.get(pvwatt_api_url, params=pv_parameters)
    pv_data = json.loads(pv.text)
    # print(pv.headers)

    e = requests.get(elecRate_api_url, params=elec_parameters)
    e_data = json.loads(e.text)
    # print(e.headers)

    #pull this data only if there are no errors. 
    if(len(pv.json()["errors"]) == 0 and len(e.json()["errors"]) == 0):
        
        #get required data and serve it back as ints
        ac_annual = changeFloatToInts(pv_data["outputs"]["ac_annual"])
        ac_monthly = changeFloatToInts(pv_data["outputs"]["ac_monthly"])
        solrad_annual = changeFloatToInts(pv_data["outputs"]["solrad_annual"])
        solrad_monthly = changeFloatToInts(pv_data["outputs"]["solrad_monthly"])

        commerc_rate = e_data["outputs"]["commercial"]
        resi_rate = e_data["outputs"]["residential"]
        indus_rate = e_data["outputs"]["industrial"]
        data = {
            "ac_annual" : ac_annual,
            "ac_monthly" : ac_monthly,
            "solrad_annual" : solrad_annual,
            "solrad_monthly" : solrad_monthly,
            "commercial" : commerc_rate,
            "residential" : resi_rate,
            "industrial" : indus_rate
        }

        sendDatatoCSV(pv_data["outputs"])

        return json.jsonify(data)
    else:
        return "ERROR: " + r.json()["errors"][0] + "\nWARNING: " + r.json()["warnings"][0]


def sendDatatoCSV(data):
    SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
    storedCSV = os.path.join(SITE_ROOT, 'historicalAPICalls.csv')
    csv_path = Path(storedCSV)
    if(type(data) is dict):
        if(csv_path.exists()):
            print('appending file: ' + storedCSV)
            with open(storedCSV, 'a') as csv_file:
                print('inside file')
                writer = csv.writer(csv_file)
                writer.writerow(["time",datetime.datetime.now()])
                for key, value in data.items():
                    writer.writerow([key, value])
                writer.writerow(" ")
            csv_file.close()
        else:
            print('writing new file')
            with open(storedCSV, 'w') as csv_file:
                writer = csv.writer(csv_file)
                writer.writerow(["time",datetime.datetime.now()])
                for key, value in data.items():
                    writer.writerow([key, value])
                writer.writerow(" ")
            csv_file.close()
    

#pull csv of outputs & maintain historical records of required outputs from each API call
@app.route("/downloadRecords") 
def downloadCSV():
    return send_file('historicalAPICalls.csv',
                     mimetype='text/csv',
                     attachment_filename='Historical_API_Calls.csv',
                     as_attachment=True)
    

#helper method to simplify data
def changeFloatToInts(data):
    if type(data) is list:
        newList = []
        for element in data: 
            newList.append(int(element))
        return newList
    elif type(data) is float: 
        return int(data)




if __name__ == "__main__":
    app.run()