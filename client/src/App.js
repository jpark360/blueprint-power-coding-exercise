import React, { Component } from "react";
import "./index.css";
import HomePage from "./Components/Home/HomePage";
import PullAPI from "./Components/Results/PullAPI";

class App extends Component {
    constructor(props) {
      super(props);
      this.state = { 
        formSubmitted: false,
        queryURL: "",
        data: ""
      };
    }

    //pull data from homepage to send to results
    homepageCallback = (homePageData) => {
        this.setState({ 
          formSubmitted: homePageData.submitted,
          queryURL: homePageData.queryURL,
          data: homePageData.inputs
        });
    }

    resultsCallback = (resultsData) => {
      this.setState({
        formSubmitted: !resultsData
      })
    }

    //Render the homepage. If form submitted, render the results page.
    render() {
      return (
        <div id="App">
          <HomePage appCallback={this.homepageCallback} />
          {this.state.formSubmitted === true && <PullAPI url={this.state.queryURL} elecRate = {this.state.data.rate} appCallback={this.resultsCallback}/>}
        </div>
      );
    }
  
}

export default App;