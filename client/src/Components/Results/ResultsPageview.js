import React, { Component } from "react";
import ACContainer from "./acContainer";
import SolRadContainer from "./solRadContainer";
import ValueContainer from "./valueContainer";
import "../../index.css";
 
class ResultsPageview extends Component {
  constructor(props) {
      super(props);
      this.state = {
        resetPressed: false
        };
    }

    //get the total value generated using the electric rate data
    //returns a value
    getValueAnnual = () => {
      return this.props.elecRate * this.props.data.ac_annual
    }

    //get the monthly values generated using the electric rate data
    //returns an array
    getValueMonthly = () => {
      return this.props.data.ac_monthly.map((x) => {
        return x * this.props.elecRate; 
      });
    }

    //sends the state of the reset button up to results page
    sendDataBackUpToPullAPI = () => {
      this.props.callBackFromPullAPI(this.state.resetPressed);
    }
    
    //changes resetPressed when reset button is pressed. 
    //Calls sendDataBackUpToPullAPI
    resetClick = () => {
      this.setState({
        resetPressed: true
      }, () => {
        this.sendDataBackUpToPullAPI();
      })
    }


    /*
      render the graphs and summary values for AC, Solar Radiation,
      and the Value Generated. Also display a table with all the data
      below the graphs. Need to revisit to break this render into
      smaller components
    */
    render() {
      if(this.props.resultsLoaded) {
        const months=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
        const vals = this.getValueMonthly();
        const annualVal = this.getValueAnnual();

        const monthTable = months.map((month, i) => <td key={"mth"+i}>{month}</td> );
        const acTable = this.props.data.ac_monthly.map((ac, i) => <td key={"ac"+i}>{ac}</td> );
        const solTable = this.props.data.solrad_monthly.map((sol, i) => <td key={"sol"+i}>{sol}</td>);
        const valTable = vals.map((val, i) => <td key={"val"+i}>{val}</td>);
        return (
            <div id="resultsContainer">
              <ACContainer xaxis={months} annual={this.props.data.ac_annual} monthly={this.props.data.ac_monthly} loaded={this.props.resultsLoaded}/>
              <SolRadContainer xaxis={months} annual={this.props.data.solrad_annual} monthly={this.props.data.solrad_monthly} />
              <ValueContainer xaxis={months} annual={annualVal} monthly={vals} />
              <div id="table-div">
                <table id="data-table">
                  <tbody>
                    <tr id="month-col">
                      <th><br />Month</th>
                      {monthTable}
                    </tr>
                    <tr id="ac-col">
                      <th>AC Energy<br />(kwh)</th>
                      {acTable}
                    </tr>
                    <tr id="sol-col">
                      <th>Solar Radiation<br />(kwh/m2/day)</th>
                      {solTable}
                    </tr>
                    <tr id="val-col">
                      <th>Value<br />($)</th>
                      {valTable}
                    </tr>
                  </tbody>
                </table>
              </div>
              <div id="button-div">
                <button id="reset-button" onClick={this.resetClick}>Reset</button>
                <a href="http://localhost:5000/downloadRecords"><button id="csv-button" type="button">Download CSV</button></a>
              </div>
              
            </div>
            
          );
        } else {
        return (
          <div>
            Loading...
          </div>
        )
      }
  }
}
 
export default ResultsPageview;