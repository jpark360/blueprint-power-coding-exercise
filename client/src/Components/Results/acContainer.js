import React, { Component } from "react";
import {Bar} from "react-chartjs-2";
import "../../index.css";
 
class ACContainer extends Component {
    
    //uses Chart.js to graph the AC data. Shows the graph 
    //along with summary information.
    //Need to investigate why the dataset parameters are not working 
    //correctly. The graph should be Red.
    render() {
        if(this.props.xaxis.length > 0) {
            var dataTable = {
                labels: this.props.xaxis,
                datasets: [
                    {
                        label: "AC Annual",
                        backgroundColor: "rgba(143,204,201,0.5)",
                        borderColor: "rgba(143,204,201,0.5)",
                        data: this.props.monthly
                    }
                ]
            };
            return (
                <div id="ac-container">
                    <div className="summary-info">
                        <span className="summary-title">
                            AC ANNUAL:
                            <br/>
                            (kWh)
                        </span>
                        <br />
                        {this.props.annual}
                    </div>
                    <Bar
                        data={dataTable}
                        width={100}
                        height={100}
                    />
                </div>
            );
        } else {
            return(
                <div>
                    Chart loading...
                </div>
            )       
        }   
    }
}
 
export default ACContainer;