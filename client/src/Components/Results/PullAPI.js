import React, { Component } from "react";
import ResultsPageview from "./ResultsPageview";
import "../../index.css";
 
class PullAPI extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: ""
        };    
    }

    callBackFromResultsView = (dataFromResultsView) => {
      this.sendDataToApp(dataFromResultsView);
    }

    sendDataToApp = (dataFromResults) => {
      this.props.appCallback(dataFromResults)
    }

    //fetch the data from the API using queryURL from App
    componentDidMount() {
      fetch(this.props.url)
        .then(response => response.json())
        .then(response => {
          this.setState({ data: response});
        })
    }

    //if data is fetched, show results page. Otherwise, show loading screen
    render() {
      this.isDoneLoading = this.state.data !== ""
      if(this.isDoneLoading) {
        return (
          <div>
            <ResultsPageview data={this.state.data} elecRate={this.props.elecRate} resultsLoaded={this.isDoneLoading} callBackFromPullAPI={this.callBackFromResultsView}/>
          </div>
        );
      } else {
        return (
          <div>
            Loading...
          </div>
        )
      }
    }
}
 
export default PullAPI;