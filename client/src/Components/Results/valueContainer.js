import React, { Component } from "react";
import {Bar} from "react-chartjs-2";
import "../../index.css";
 
class ValueContainer extends Component {

    //uses Chart.js to graph the Value Generated data. 
    //Shows the graph along with summary information.
    //Need to investigate why the dataset parameters are not working 
    //correctly. The graph should be Blue.
    render() {
        if(this.props.xaxis.length > 0) {
            var dataTable = {
                labels: this.props.xaxis,
                datasets: [
                    {
                        label: "Annual Value",
                        backgroundColor: "rgba(114,255,246,0.5)",
                        borderColor: "rgba(114,255,246,0.5)",
                        data: this.props.monthly
                    }
                ]
            };
            return (
                <div id="value-container">
                    <div className="summary-info">
                        <span className="summary-title">
                            ANNUAL VALUE: 
                            <br />
                            ($)
                        </span>
                        <br />
                        {this.props.annual}
                    </div>
                    <Bar
                        data={dataTable}
                        width={100}
                        height={100}
                    />
                </div>
            );
        } else {
            return(
                <div>
                    Chart loading...
                </div>
            )        
        }   
    }
}
 
export default ValueContainer;
                    

                   