import React, { Component } from "react";
import {Bar} from "react-chartjs-2";
import "../../index.css";
 
class SolRadContainer extends Component {

    //uses Chart.js to graph the Solar Radiation data. 
    //Shows the graph along with summary information.
    //Need to investigate why the dataset parameters are not working 
    //correctly. The graph should be Green.
    render() {
        if(this.props.xaxis.length > 0) {
            var dataTable = {
                labels: this.props.xaxis,
                datasets: [
                    {
                        label: "Solar Radiation Annual",
                        backgroundColor: "rgba(203,253,223,0.5)",
                        borderColor: "rgba(203,253,223,0.5)",
                        data: this.props.monthly
                    }
                ]
            };
            return (
                <div id="solrad-container">
                    <div className="summary-info">
                        <span className="summary-title">
                            SOLAR RADIATION ANNUAL: 
                            <br />
                            (kWh/m2/day)
                        </span>
                        <br/> 
                        {this.props.annual}
                    </div>
                    <Bar
                        data={dataTable}
                        width={100}
                        height={100}
                    />
                </div>
            );
        } else {
            return(
                <div>
                    Chart loading...
                </div>
            )         
        }   
    }
}
 
export default SolRadContainer;