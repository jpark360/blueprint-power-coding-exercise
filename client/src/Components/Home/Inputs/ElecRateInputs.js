import React, { Component } from "react";
import Select from "react-select";
 
class ElecRateInputs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rateTypeSelect: "",
      rtVal: "residential",
      rate: "2"
      };
  }

  sendDataToParent = () => {
    this.elecData = {
      "rateType" : this.state.rtVal,
      "rate" : this.state.rate
    }
    this.props.callBackFromHome(this.elecData);
  }

  updateRateType = (evt) => {
    this.setState({
      rateTypeSelect: evt,
      rtVal: evt.value
    }, () => {
      this.sendDataToParent()
    });
  }

  updateRate = (evt) => {
    this.setState({
      rate: evt.target.value
    }, () => {
      this.sendDataToParent()
    });
  }

  componentDidMount() {
    //using the locData props, fetch elecRateData from localhost:5000/retailElecRate/<lat>/<lon>. 
    //check conditional of what rate type is selected (default to residential)
    //change the this.state.rate to corresponding rate
  }

  shouldComponentUpdate() {
    //check to see if fetched elecRate is different than current this.state.rate. If so, update the component with the correct rate data.
    return true;
  }

  render() {
    let rateTypeOptions = [
              { value: "residential", label: "Residential" },
              { value: "commercial", label: "Commercial" },
              { value: "industrial", label: "Industrial" }
    ];

    return (
      <div id="elecInputsContainer">
      <div id="rateTypeSelect">
      <label className="inputSelect">
          Rate Type:
          <Select
            id="rate-type-select"
            ref={(ref) => { this.select = ref; }}
            options={rateTypeOptions}
            simpleValue
            clearable={true}
            name="rateSelect"
            value={this.state.rateTypeSelect}
            onChange={this.updateRateType}
            
          />
        </label></div>
        
        <div id="rateInput">
        <label className="inputVal">
          Rate ($/kWh):
          <input 
            value={this.state.rate} 
            onChange={e => this.updateRate(e)}
            type="text" 
            name="rate" />
        </label></div>
        
      </div>
    );
  }
}
 
export default ElecRateInputs;