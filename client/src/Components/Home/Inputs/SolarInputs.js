import React, { Component } from "react";
import Select from "react-select";

/*
  React-Select documentation states that you can change
  the styling of the Select Component with the following 
  import. However, this import does not work for me. 
*/
// import "react-select/dist/react-select.css";
 
class SolarInputs extends Component {
    constructor(props) {
      super(props);
      this.state = { 
        moduleSel: "",
        arraySel: "",
        dcVal: "25",
        moVal: "0",
        arVal: "0",
        slVal: "13",
        tiVal: "25",
        azVal: "180",
        rateTypeSelect: "",
        rtVal: "residential",
        rate: "2"
        };
    }

    //send the input data up to InputsArea
    sendDataToInputsArea = () => {
      this.solarData = {
        "dc" : this.state.dcVal,
        "mt" : this.state.moVal,
        "at" : this.state.arVal,
        "sl" : this.state.slVal,
        "ti" : this.state.tiVal,
        "az" : this.state.azVal,
        "rateType" : this.state.rtVal,
        "rate" : this.state.rate
      }
      this.props.callBackFromHome(this.solarData);
    }

    /*
      Following methods update state and call sendDataToInputs.
      Need to revisit to abstract these even more.
    */
    updateVal = (state, evt) => {
      this.setState({
        state: evt.target.value
      }, () => {
        this.sendDataToInputsArea()
      });
    }

    updateDCVal = (evt) => {
      this.setState({
        dcVal: evt.target.value
      }, () => {
        this.sendDataToInputsArea()
      });
    }

    updateSLVal = (evt) => {
      this.setState({
        slVal: evt.target.value
      }, () => {
        this.sendDataToInputsArea()
      });
    }

    updateTIVal = (evt) => {
      this.setState({
        tiVal: evt.target.value
      }, () => {
        this.sendDataToInputsArea()
      });
    }

    updateAZVal = (evt) => {
      this.setState({
        azVal: evt.target.value
      }, () => {
        this.sendDataToInputsArea()
      });
    }

    updateModule = (evt) => {
      this.setState({
        moduleSel: evt,
        moVal: evt.value
      }, () => {
        this.sendDataToInputsArea()
      });
    }

    updateArray = (evt) => {
      this.setState({
        arraySel: evt,
        arVal: evt.value
      }, () => {
        this.sendDataToInputsArea()
      });
    }

    updateRateType = (evt) => {
      this.setState({
        rateTypeSelect: evt,
        rtVal: evt.value
      }, () => {
        this.sendDataToInputsArea()
      });
    }

    updateRate = (evt) => {
      this.setState({
        rate: evt.target.value
      }, () => {
        this.sendDataToInputsArea()
      });
    }

    //render the input areas
    render() {
      let moduleOptions = [
                  { value: "0", label: "Standard" },
                  { value: "1", label: "Premium" },
                  { value: "2", label: "Thin Film" }
                ];
      let arrayOptions = [
                { value: "0", label: "Fixed - Open Rack" },
                { value: "1", label: "Fixed - Roof Mount" },
                { value: "2", label: "1-Axis" },
                { value: "3", label: "1-Axis Backtracking" },
                { value: "4", label: "2-Axis" }
              ];
      let rateTypeOptions = [
                { value: "residential", label: "Residential" },
                { value: "commercial", label: "Commercial" },
                { value: "industrial", label: "Industrial" }
      ];

      return (

        <div id="solarInputsContainer">
          <div id="dcInput">
            <label className="inputVal">
              DC system size: (kW):
              <input 
                value={this.state.dcVal}
                onChange={e => this.updateDCVal(e)} 
                type="text" 
                name="dc" />
            </label>
          </div>

          <div id="modInput">
            <label className="inputSelect">
              Module Type:
              <Select
                id="module-select"
                ref={(ref) => { this.select = ref; }}
                options={moduleOptions}
                simpleValue
                clearable={true}
                name="moduleSelect"
                value={this.state.moduleSel}
                onChange={this.updateModule}
              />
            </label>
          </div>

          <div id="arrInput">
            <label className="inputSelect">
              Array Type:
              <Select
                id="array-select"
                ref={(ref) => { this.select = ref; }}
                options={arrayOptions}
                simpleValue
                clearable={true}
                name="arraySelect"
                value={this.state.arraySel}
                onChange={this.updateArray} 
              />
            </label>
          </div>

          <div id="slInput">
            <label className="inputVal">
              System Losses (%):
              <input 
                value={this.state.slVal}
                onChange={e => this.updateSLVal(e)}
                type="text" 
                name="sl" />
            </label>
          </div>
          
          <div id="tiInput">
            <label className="inputVal">
              Tilt (deg):
              <input 
                value={this.state.tiVal}
                onChange={e => this.updateTIVal(e)}
                type="text" 
                name="ti" />
            </label>
          </div>

          <div id="azInput">
            <label className="inputVal">
              Azimuth (deg):
              <input 
                value={this.state.azVal}
                onChange={e => this.updateAZVal(e)}
                type="text" 
                name="az" />
            </label>
          </div>

          <div id="rateTypeSelect">
            <label className="inputSelect">
                Rate Type:
                <Select
                  id="rate-type-select"
                  ref={(ref) => { this.select = ref; }}
                  options={rateTypeOptions}
                  simpleValue
                  clearable={true}
                  name="rateSelect"
                  value={this.state.rateTypeSelect}
                  onChange={this.updateRateType}
                />
              </label>
          </div>
          
          <div id="rateInput">
            <label className="inputVal">
              Rate ($/kWh):
              <input 
                value={this.state.rate} 
                onChange={e => this.updateRate(e)}
                type="text" 
                name="rate" />
            </label>
          </div>
        </div>
      );
    }
}
 
export default SolarInputs;