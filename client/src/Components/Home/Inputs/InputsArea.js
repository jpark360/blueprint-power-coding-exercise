import React, { Component } from "react";
import SolarInputs from "./SolarInputs";

class InputsArea extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listDataFromSolar: {
                "dc": "25",
                "mt": "0",
                "sl": "13",
                "at": "0",
                "ti": "25",
                "az": "180",
                "rateType" : "residential",
                "rate" : "2"
            },
            submitPressed: false
        };    
    }

    //gets data from SolarInputs, updates listDataFromSolar, 
    //and calls sendDataToHomePage
    solarCB = (dataFromLoc) => {
        this.setState({ listDataFromSolar: dataFromLoc }, () => {
            this.sendDataToHomePage();
        });
    }

    //sends Input data & submit button status up to Homepage
    sendDataToHomePage = () => {
        this.props.inputCallBack({
            "solar" : this.state.listDataFromSolar,
            "submitted": this.state.submitPressed
        });
    }

    //On Submit Button press, update submit button status state
    //and call sendDatatoHomepage
    handleClick = () => {
        this.setState({ submitPressed: true }, () => {
            this.sendDataToHomePage(this.url);
        });
    }


    //Render the Inputs and the submit button
    render() {
        return (
        <div id="inputArea">
            <form id="input-form" onSubmit={e => this.submitLocation(e)}>
            <SolarInputs callBackFromHome={this.solarCB} />
            <div id="submitButton">
                    <button 
                        type="button" 
                        onClick={this.handleClick}
                        form="input-form" 
                        value="Submit">Submit </button>
                </div>
            </form>
        </div>
        );
    }
}
 
export default InputsArea;