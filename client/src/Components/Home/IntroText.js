import React, { Component } from "react";
 
class IntroText extends Component {
  render() {
      return (
        <div id="introText">
            <h1>
                Blueprint Power Modeling Tool
            </h1>
            <div id="bodyText">
            <p>
                This tool notifies users on the amount of solar energy generated from a given solar installation and the expected value of this generated electricity over the course of a year. 
            </p>
            <p>
                To begin, enter the latitude and longitude coordinates of the location you are interested in. 
            </p>
            </div>
        </div>
      );
  }
}
 
export default IntroText;