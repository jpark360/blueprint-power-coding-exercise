import React, { Component } from "react";
 
class Location extends Component {
    constructor(props) {
      super(props);
      this.state = {
        latVal: "",
        lonVal: "",
        locSubmitPressed: false
        };
    }

    //sends data up to MapArea Component
    sendDataToMapArea = () => {
      this.locData = {
        "lat" : this.state.latVal,
        "lon" : this.state.lonVal,
        "locAvail": this.state.locSubmitPressed
      }
      this.props.callBackFromMap(this.locData);
    }

    //updates the latitude state with input
    updateLatVal = (evt) => {
      this.setState({
        latVal: evt.target.value
      });
    }

    //updates the longitude state with input
    updateLonVal = (evt) => {
      this.setState({
        lonVal: evt.target.value
      });
    }

    //submits the location data and calls sendDataToMapArea
    submitLocation = (e) => {
      e.preventDefault();
      this.setState({ locSubmitPressed: true }, () => {
        this.sendDataToMapArea();
      });
    }

    //render the latitude and longitude input areas along with submit button
    render() {
      return (
        <div id="locationInputsContainer">

          <form id="locInputs" onSubmit={e => this.submitLocation(e)}>
            Latitude:
            <input 
              value={this.state.latVal} 
              onChange={e => this.updateLatVal(e)}
              type="text" 
              name="lat"
              placeholder={"e.g. 40.74"} />
            <span> </span>
            Longitude:
            <input 
              value={this.state.lonVal} 
              onChange={e => this.updateLonVal(e)}
              type="text" 
              name="lon" 
              placeholder={"e.g. -73.99"} />
            <button type="submit" form="locInputs" value="Map" >Map</button>
            
          </form>

        </div>
      );
    }
}

export default Location;