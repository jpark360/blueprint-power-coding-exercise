import React, { Component } from "react";
import "../../../index.css";
import { Map, TileLayer, Circle } from "react-leaflet"

class Leafmap extends Component {
    constructor(props) {
      super(props)
      this.state = {
        zoom: 15
      }
    }

    //Using React-Leaflet, takes lat/lon data and displays a map. 
    //Draws a circle to indicate the queried area on the map.
    render() {
      const position = [this.props.lat, this.props.lng];
      return (
          <div id="mapid">
              <Map center={position} zoom={this.state.zoom}>
                  <TileLayer
                      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                      attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                  />
                  <Circle center={position} radius={100} />
              </Map>
          </div>
        
      );
    }
}

export default Leafmap;
