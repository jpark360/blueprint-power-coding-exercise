import React, { Component } from "react";
import Location from "./Location";
import Leafmap from "./Leafmap";
 
class MapArea extends Component {
    constructor(props) {
      super(props);
      this.state = {
          dataFromLocation: null,
          locationAvailable: false
      };    
    }

    //pull location data from Location component
    callBackFromLoc = (dataFromLoc) => {
      this.setState({ 
        dataFromLocation: dataFromLoc,
        locationAvailable: dataFromLoc.locAvail
        }, () => {
          this.sendDataToHomepage();
      });
    }

    //send location data up to Homepage
    sendDataToHomepage = () => {
      this.props.callBack({
          "locData" : this.state.dataFromLocation,
          "locAvail": this.state.locationAvailable
      });
    }

    //Render lat/lon inputs. Show map only when lat/lon is submitted
    render() {
        return (
          <div id="mapArea">
              <Location callBackFromMap={this.callBackFromLoc} /> 
              {this.state.locationAvailable === true && <Leafmap lat={this.state.dataFromLocation.lat} lng={this.state.dataFromLocation.lon}/>}     
          </div>
        );
    }
}
 
export default MapArea;
