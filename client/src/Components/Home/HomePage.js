import React, { Component } from "react";
import IntroText from "./IntroText";
import MapArea from "./Maps/MapArea";
import InputsArea from "./Inputs/InputsArea";

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listDataFromLocation: null,
            listDataFromSolar: {
                "dc": "25",
                "mt": "0",
                "sl": "13",
                "at": "0",
                "ti": "25",
                "az": "180",
                "rateType" : "residential",
                "rate" : "2"
            },
            locationAvailable: false,
            submitPressed: false,
            url: ""
        };    
    }

    //pull location data from mapArea component
    mapAreaDataCallback = (dataFromMapArea) => {
        this.setState({ 
            listDataFromLocation: dataFromMapArea.locData,
            locationAvailable: dataFromMapArea.locAvail
            });
    }

    //pull inputs data from input component. 
    //also get information on whether inputs form was submitted
    inputDataCallback = (dataFromInput) => {
        this.setState({ 
            listDataFromSolar: dataFromInput.solar,
            submitPressed: dataFromInput.submitted 
            }, () => {
                this.sendDataToApp();
            });
    }

    //send inputs, url, and submit button data back up to App
    sendDataToApp = () => {
        this.url = String.format("http://localhost:5000/pvwatts/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}", this.state.listDataFromLocation.lat, this.state.listDataFromLocation.lon, this.state.listDataFromSolar.dc, this.state.listDataFromSolar.mt, this.state.listDataFromSolar.sl, this.state.listDataFromSolar.at, this.state.listDataFromSolar.ti, this.state.listDataFromSolar.az );
        this.setState({
            url: this.url
        }, () => {
            this.props.appCallback({
            "submitted" : this.state.submitPressed,
            "queryURL" : this.url,
            "inputs" : this.state.listDataFromSolar
        });
        })
    }

    //Render the map area where we query for location. Render input area only if location data is submitted. 
    render() {
        return (
        <div id="homeContainer">
            <div id="top">
                <img id="logo" alt="bp-logo" src="https://www.blueprintpower.com/img/bp-logo-white.png" />
                <IntroText />
            </div>
            <div id="left-home">
                <MapArea callBack={this.mapAreaDataCallback}/>
            </div>
            <div id="right-home">
                {this.state.locationAvailable === true && 
                <InputsArea inputCallBack={this.inputDataCallback} locData={this.state.listDataFromLocation} solData={this.state.listDataFromSolar}/>}
            </div>
        </div>
        );
    }
}
 
export default HomePage;

//helper function to format String in Javascript like in Python
if (!String.format) {
    String.format = function(format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function(match, number) { 
        return typeof args[number] !== "undefined"
            ? args[number] 
            : match
        ;
        });
    };
}